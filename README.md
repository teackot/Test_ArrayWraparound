# Time test of array wraparound algorithms

This program compares array elements access time with 3 different wraparound algorithms:

1. conditional (checks if the index is out of bounds, and wraps it if it is)
2. mod operator (arr[idx % size])
3. fast bitwise mod (arr[idx & (size - 1)])

Program draws graphs of total access time (Y axis) dependency on array size (X axis)
using ftxui library (no runtime dependencies needed).

Make sure your terminal and fonts support printing braille dots characters.

To exit the program press Ctrl+C

### Conclusion

Difference in time is most noticeable on big sized and/or frequently accessed arrays.

The fastest algorithm...
