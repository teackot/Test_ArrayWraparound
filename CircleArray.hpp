#pragma once

class CircleArray {
private:
    unsigned char *arr;
    int arr_size;
    int arr_size_m1;

public:
    explicit CircleArray(int size) {
        this->arr_size = size;
        this->arr_size_m1 = size - 1;
        this->arr = new unsigned char [size];
    }

    [[nodiscard]] int size() const {
        return arr_size;
    }

    [[nodiscard]] unsigned char get_conditional(int idx) const {
        if (idx < 0) {
            return arr[arr_size + idx];
        } else if (idx >= arr_size) {
            return arr[idx - arr_size];
        }

        return arr[idx];
    }

    [[nodiscard]] unsigned char get_mod(int idx) const {
        return arr[((idx % arr_size) + arr_size) % arr_size];
    }

    // only if arr_size == 2^n
    [[nodiscard]] unsigned char get_bitwise(int idx) const {
        return arr[idx & arr_size_m1];
    }
};
