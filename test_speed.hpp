#pragma once

#include <chrono>

#include "CircleArray.hpp"

double test_speed(CircleArray &arr, unsigned char (CircleArray::*f)(int) const, int count) {
    int size = count;
    int over = size / 4;

    auto start = std::chrono::steady_clock::now();

    for (int i = -over; i < size + over; i++) {
        [[maybe_unused]] volatile auto n = (arr.*f)(i);
    }

    auto end = std::chrono::steady_clock::now();

    return std::chrono::duration <double, std::milli> (end - start).count();
}
