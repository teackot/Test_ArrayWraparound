#include <iostream>
using std::cout, std::endl, std::to_string;

#include <cmath>

#include <algorithm>

#include <chrono>
using namespace std::chrono;
using std::milli;

#include <ftxui/dom/elements.hpp>
#include <ftxui/screen/screen.hpp>
#include <ftxui/component/component.hpp>
#include <ftxui/component/component_base.hpp>
#include <ftxui/component/event.hpp>
#include <ftxui/component/mouse.hpp>
#include <ftxui/component/screen_interactive.hpp>
#include <ftxui/dom/canvas.hpp>
#include <ftxui/screen/color.hpp>

#include "CircleArray.hpp"
#include "test_speed.hpp"

int main() {
    auto arr = CircleArray(pow(2, 30));

    cout << "Calculating time of mod wraparound..." << endl;
    std::vector<double> mod_ys(30);
    for (int x = 0; x < 30; x++) {
        mod_ys[x] = test_speed(arr, &CircleArray::get_mod, pow(2, x));
    }

    cout << "Calculating time of conditional wraparound..." << endl;
    std::vector<double> conditional_ys(30);
    for (int x = 0; x < 30; x++) {
        conditional_ys[x] = test_speed(arr, &CircleArray::get_conditional, pow(2, x));
    }

    cout << "Calculating time of bitwise mod wraparound..." << endl;
    std::vector<double> bitwise_ys(30);
    for (int x = 0; x < 30; x++) {
        bitwise_ys[x] = test_speed(arr, &CircleArray::get_bitwise, pow(2, x));
    }

    // -----------------------------------

    const auto dims = ftxui::Dimension::Full();

    const int    width   = dims.dimx * 2 / 2;
    const int    height  = dims.dimy * 4 - 2 * 4;
    const double scale_x = double(width) / 30;
    const double scale_y = height / std::max({
        mod_ys[29],
        conditional_ys[29],
        bitwise_ys[29],
    });

    auto screen = ftxui::ScreenInteractive::Fullscreen();

    auto renderer_mod = ftxui::Renderer([&] {
        auto c = ftxui::Canvas(width, height);

        c.DrawText(0, 0, "Mod         (max time = " + std::to_string(mod_ys[29]) + "ms)");

        for (int x = 0; x < 29; x++)
            c.DrawPointLine(static_cast<int>(scale_x * x),
                            static_cast<int>(height - 1 - scale_y * mod_ys[x]),
                            static_cast<int>(scale_x * (x + 1)),
                            static_cast<int>(height - 1 - scale_y * mod_ys[x + 1])
            );

        return canvas(std::move(c));
    });

    auto renderer_conditional = ftxui::Renderer([&] {
        auto c = ftxui::Canvas(width, height);

        c.DrawText(0, 0, "Condition   (max time = " + std::to_string(conditional_ys[29]) + "ms)");

        for (int x = 0; x < 29; x++)
            c.DrawPointLine(static_cast<int>(scale_x * x),
                            static_cast<int>(height - 1 - scale_y * conditional_ys[x]),
                            static_cast<int>(scale_x * (x + 1)),
                            static_cast<int>(height - 1 - scale_y * conditional_ys[x + 1])
            );

        return canvas(std::move(c));
    });

    auto renderer_bitwise = ftxui::Renderer([&] {
        auto c = ftxui::Canvas(width, height);

        c.DrawText(0, 0, "Bitwise mod (max time = " + std::to_string(bitwise_ys[29]) + "ms)");

        for (int x = 0; x < 29; x++)
            c.DrawPointLine(static_cast<int>(scale_x * x),
                            static_cast<int>(height - 1 - scale_y * bitwise_ys[x]),
                            static_cast<int>(scale_x * (x + 1)),
                            static_cast<int>(height - 1 - scale_y * bitwise_ys[x + 1])
            );

        return canvas(std::move(c));
    });

    [[maybe_unused]] auto renderer_table = ftxui::Renderer([&] {
        auto c = ftxui::Canvas(width, height);

        c.DrawText(0, 0, "Table");

        for (int x = 0; x < 30; x++) {
            c.DrawText(0, 4 * x + 4 * 3, "2^" + to_string(x) + " | "
                                   + to_string(mod_ys[x]) + " | "
                                   + to_string(conditional_ys[x]) + " | "
                                   + to_string(bitwise_ys[x]) + " |"
            );
        }

        return canvas(std::move(c));
    });

    int selected_tab = 0;
    auto tab = ftxui::Container::Tab({
        renderer_mod,
        renderer_conditional,
        renderer_bitwise,
        renderer_table,
    }, &selected_tab);

    auto tab_with_event_catcher = ftxui::CatchEvent(tab, [&](ftxui::Event e) {
        if (e == ftxui::Event::Character('q')) {
            screen.ExitLoopClosure();
        }
        return false;
    });

    std::vector<std::string> tab_titles = {
            "With mod",
            "With condition",
            "Bitwise mod",
            "Comparison table",
    };
    auto tab_toggle = ftxui::Menu(&tab_titles, &selected_tab);

    auto component = ftxui::Container::Horizontal({
        tab_with_event_catcher,
        tab_toggle,
    });

    // Add some separator to decorate the whole component:
    auto component_renderer = Renderer(component, [&] {
        return ftxui::hbox({
            tab_with_event_catcher->Render(),
            ftxui::separator(),
            tab_toggle->Render(),
        }) |
        ftxui::border;
    });

    screen.Loop(component_renderer);

    return 0;
}
